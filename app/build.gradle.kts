import com.rariki.buildsrc.AppConfig
import com.rariki.buildsrc.Projects
import com.rariki.buildsrc.Libs
import com.rariki.buildsrc.Versions

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
}

apply(from = "$rootDir/base-gradle.kts")

android {
    namespace = "com.rariki.baseprojectandroid"

    defaultConfig {
        applicationId = "com.rariki.baseprojectandroid"
        versionCode = AppConfig.versionCode
        versionName = AppConfig.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = Versions.KOTLIN_COMPILER_EXTENSION
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

dependencies {
    /**
     * CORE
     */
    implementation(project(Projects.CORE_UI))
    implementation(project(Projects.CORE_DATA))
    implementation(project(Projects.CORE_LANGUAGE))
    implementation(project(Projects.CORE_NETWORK))
    implementation(project(Projects.CORE_NAVIGATION))

    implementation(Libs.androidCoreKtx)
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.6.1")
    implementation(Libs.composeActivity)
    implementation(platform(Libs.composeBoM))
    implementation(Libs.compose)
    implementation(Libs.composeGraphic)
    implementation(Libs.composeToolingPreview)
    implementation(Libs.composeMaterial3)
    implementation(Libs.composeNavigation)

    implementation(Libs.koinCompose)

    implementation(Libs.timber)

    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
    androidTestImplementation(platform("androidx.compose:compose-bom:2023.03.00"))
    androidTestImplementation("androidx.compose.ui:ui-test-junit4")
    debugImplementation("androidx.compose.ui:ui-tooling")
    debugImplementation("androidx.compose.ui:ui-test-manifest")
}