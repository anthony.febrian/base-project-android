package com.rariki.buildsrc

/**
 * Collection of library
 */
object Libs {
    /**
     * Accompanist
     */
    const val accompanistPermission = "com.google.accompanist:accompanist-permissions:${Versions.ACCOMPANIST}"


    const val androidCoreKtx = "androidx.core:core-ktx:${Versions.ANDROID_CORE_KTX}"

    /**
     * Coil
     */
    const val coilCompose = "io.coil-kt:coil-compose:${Versions.COIL}"



    /**
     * Compose
     */
    const val composeBoM = "androidx.compose:compose-bom:${Versions.COMPOSE_BOM}"
    const val compose = "androidx.compose.ui:ui"
    const val composeGraphic = "androidx.compose.ui:ui-graphics"
    const val composeMaterial3 = "androidx.compose.material3:material3"
    const val composeWindowSizedMaterial3 = "androidx.compose.material3:material3-window-size-class"
    const val composeMaterialIcon =
        "androidx.compose.material:material-icons-core"
    const val composeMaterialIconExtended =
        "androidx.compose.material:material-icons-extended"
    const val composeToolingPreview = "androidx.compose.ui:ui-tooling-preview"
    const val composeNavigation =
        "androidx.navigation:navigation-compose:${Versions.COMPOSE_NAVIGATION}"
    const val composeActivity = "androidx.activity:activity-compose:${Versions.COMPOSE_ACTIVITY}"
    const val composeDebugUITooling = "androidx.compose.ui:ui-tooling"

    /**
     * Coroutines
     */
    const val coroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.COROUTINES}"

    /**
     * KOIN
     */
    const val koin = "io.insert-koin:koin-core:${Versions.KOIN}"
    const val koinAndroid = "io.insert-koin:koin-android:${Versions.KOIN}"
    const val koinCompose = "io.insert-koin:koin-androidx-compose:${Versions.KOIN_COMPOSE}"

    /**
     * Lifecycle
     */
    const val lifecyleRuntimeCompose = "androidx.lifecycle:lifecycle-runtime-compose:${Versions.LIFECYCLE}"
    const val lifecyleViewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.LIFECYCLE}"
    const val lifecyleViewModelCompose = "androidx.lifecycle:lifecycle-viewmodel-compose:${Versions.LIFECYCLE}"
    const val lifeCycleLiveData = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.LIFECYCLE}"
    const val lifeCycleReactiveStreams = "androidx.lifecycle:lifecycle-reactivestreams-ktx:${Versions.LIFECYCLE}"


    /**
     * Retrofit
     */
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.RETROFIT}"
    const val retrofitMoshi = "com.squareup.retrofit2:converter-moshi:${Versions.RETROFIT}"

    /**
     * Room
     */
    const val roomRuntime = "androidx.room:room-runtime:${Versions.ROOM}"
    const val roomCompiler = "androidx.room:room-compiler:${Versions.ROOM}"
    const val roomRxJava2 = "androidx.room:room-rxjava2:${Versions.ROOM}"
    const val roomRxJava3 = "androidx.room:room-rxjava3:${Versions.ROOM}"
    const val roomGuava = "androidx.room:room-guava:${Versions.ROOM}"
    const val roomPaging = "androidx.room:room-paging:${Versions.ROOM}"

    /**
     * OkHttp
     */
    const val okHttpBoM = "com.squareup.okhttp3:okhttp-bom:${Versions.OKHTTP}"
    const val okHttp = "com.squareup.okhttp3:okhttp"
    const val okHttpInterceptor = "com.squareup.okhttp3:logging-interceptor"

    const val timber = "com.jakewharton.timber:timber:${Versions.TIMBER}"
}