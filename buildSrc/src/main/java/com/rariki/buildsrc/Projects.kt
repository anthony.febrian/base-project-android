package com.rariki.buildsrc

object Projects {
    const val CORE_UI = ":core:ui"
    const val CORE_DATA = ":core:data"
    const val CORE_LANGUAGE = ":core:language"
    const val CORE_NETWORK = ":core:network"
    const val CORE_NAVIGATION = ":core:navigation"
}