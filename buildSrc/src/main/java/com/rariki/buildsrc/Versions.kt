package com.rariki.buildsrc

import org.gradle.api.JavaVersion

object Versions {
    const val JVM_TARGET = "1.8"
    val JAVA_VERSION = JavaVersion.VERSION_1_8
    const val KOTLIN_COMPILER_EXTENSION = "1.4.3"

    const val ACCOMPANIST = "0.30.1"
    const val ANDROID_CORE_KTX = "1.10.1"

    const val COIL = "2.4.0"

    const val COMPOSE_BOM = "2023.08.00"
    const val COMPOSE_NAVIGATION = "2.7.0"
    const val COMPOSE_ACTIVITY = "1.7.2"

    const val COROUTINES = "1.7.3"

    const val KOIN = "3.4.3"
    const val KOIN_COMPOSE = "3.4.6"

    const val LIFECYCLE = "2.6.1"

    const val OKHTTP = "4.11.0"

    const val RETROFIT = "2.9.0"

    const val ROOM = "2.5.2"

    const val TIMBER = "5.0.1"
}