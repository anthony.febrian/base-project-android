package com.rariki.core.network

import okhttp3.Interceptor
import okhttp3.Response

class NetworkInterceptor:Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val builder = original.newBuilder()

        /**
         * You Can Add Header Or You Can modify Request Here
         */

        val request = builder.build()
        return chain.proceed(request)
    }

}