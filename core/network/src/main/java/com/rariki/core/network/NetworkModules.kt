package com.rariki.core.network

import org.koin.dsl.module

object NetworkModules {
    val modules = module {
        single {
            ApiConfig.retrofit
        }
    }
}