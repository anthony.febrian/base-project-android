import com.rariki.buildsrc.Libs
import com.rariki.buildsrc.Versions


plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
}

apply(from = "$rootDir/base-gradle.kts")

android {
    namespace = "com.rariki.core.ui"

    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = Versions.KOTLIN_COMPILER_EXTENSION
    }
}

dependencies {

    implementation(Libs.androidCoreKtx)
    implementation(platform(Libs.composeBoM))
    implementation(Libs.compose)
    implementation(Libs.composeToolingPreview)
    implementation(Libs.composeMaterial3)
    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")
}